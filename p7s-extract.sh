#!/usr/bin/env bash

#Christian Fairlie Pearson van Langendonck
#This is a script that converts a P7S signature file back into PDF

function getInput {
    if [ -n "$1" ]
    then
        input=$1
    else
        if [ -x "$(command -v kdialog)" ]
        then
            input=$(kdialog --getopenfilename . "P7S files (*.p7s)")
            if [ ! $? -eq 0 ]
            then
                echo No file selected.
                exit 1
            fi
        elif [ -x "$(command -v zenity)" ] && [ -z "$input" ]
        then
            input=$(zenity --file-selection --filename "${HOME}/")
            if [ ! $? -eq 0 ]
            then
                echo No file selected.
                exit 1
            fi
        else
            echo No compatible GUI available and no file was provided.
            echo CLI usage: $0 input_file.pdf output_file.pdf
            exit 1
        fi
    fi
}

function checkFile {
    if [ ! -f $input ]
    then
        echo No valid file selected.
        exit 1
    fi
}

function extract {
    echo "Extracting $input to $output"
    openssl smime -inform DER -verify -noverify -in $1 -out $2
    if [ $? -eq 0 ]
    then
        echo Created new file $2
        exit 0
    else
        echo No action taken
        exit 1
    fi
}

getInput $1
if [ $? -eq 0 ]
then
    checkFile
    if [ $? -eq 0 ]
    then
        if [ -z "$2" ]
        then
            output=new_extracted.pdf
        else
            output=$2
        fi
        extract $input $output
    fi
fi
