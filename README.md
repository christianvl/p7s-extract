# Compress PDF

<p>This is a simple Bash script to extract the original PDF file from a P7S signature file using Openssl

## Instructions 

<p>If the command is run without any arguments, the script will try to call a GUI to select a file. If there's no compatible GUI available, the script will terminate with an error message.

<p>One valid file must be provided.

```bash
p7s-extract input_file.p7s output_file.pdf 
```

<p>If no output is defined, the script will default to "new_extracted.pdf"

## License
<p> This software is licensed under the [AGPL3](https://www.gnu.org/licenses/agpl-3.0.html) license.
<p> ![AGPL](https://www.gnu.org/graphics/agplv3-88x31.png "AGPL3") 
